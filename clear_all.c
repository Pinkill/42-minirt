/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear_all.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/05 20:07:39 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/15 22:07:22 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	clear_objects(t_all_objects *all)
{
	t_object	*tmp;

	if (all->objects == 0)
		return ;
	while (all->objects)
	{
		tmp = all->objects;
		all->objects = all->objects->next;
		if (tmp->pos)
			free(tmp->pos);
		if (tmp->orien_vec)
			free(tmp->orien_vec);
		if (tmp->rgb)
			free(tmp->rgb);
		if (tmp->sqr_a)
			free(tmp->sqr_a);
		if (tmp->sqr_c)
			free(tmp->sqr_b);
		if (tmp->sqr_c)
			free(tmp->sqr_c);
		if (tmp->top)
			free(tmp->top);
		clear_triangle(tmp);
		free(tmp);
	}
}

static void	clear_cameras(t_all_objects *all)
{
	t_camera	*tmp;
	t_camera	*curr;

	if (all->cameras == 0)
		return ;
	curr = all->cameras->next;
	while (curr != 0 && curr != all->cameras)
	{
		tmp = curr;
		if (tmp->pos)
			free(tmp->pos);
		if (tmp->orien_vec)
			free(tmp->orien_vec);
		curr = curr->next;
		free(tmp);
	}
	if (all->cameras->pos)
		free(all->cameras->pos);
	if (all->cameras->orien_vec)
		free(all->cameras->orien_vec);
	free(all->cameras);
	all->cameras = 0;
}

static void	clear_lights(t_all_objects *all)
{
	t_light	*tmp;

	if (all->lights == 0)
		return ;
	while (all->lights)
	{
		tmp = all->lights;
		all->lights = all->lights->next;
		if (tmp->pos)
			free(tmp->pos);
		if (tmp->rgb)
			free(tmp->rgb);
		free(tmp);
	}
}

static void	clear_minirt(t_all_objects *all)
{
	if (all->minirt)
	{
		if (all->minirt->img_ptr)
			mlx_destroy_image(all->minirt->mlx_ptr, all->minirt->img_ptr);
		if (all->minirt->win_ptr)
			mlx_destroy_window(all->minirt->mlx_ptr, all->minirt->win_ptr);
		if (all->minirt->mlx_ptr)
			mlx_free_init(all->minirt->mlx_ptr);
		free(all->minirt);
	}
}

int			clear_all(t_all_objects *all)
{
	if (all)
	{
		clear_minirt(all);
		clear_lights(all);
		clear_cameras(all);
		clear_objects(all);
		if (all->ambient_lcolor)
			free(all->ambient_lcolor);
		free(all);
		all = 0;
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light_handling.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 23:10:54 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/10 18:22:22 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static float	found_first(t_all_objects *all,
	t_ray_data *data, t_light *light)
{
	t_object	*first;
	t_pos		temp;

	first = 0;
	temp = vector_subtract(*light->pos, *data->ray_o);
	*data->ray_dir = normalize(temp);
	intersect_first(all->objects, data, &first,
		points_distance(*light->pos, *data->ray_o));
	if (first == 0 && light->brightness > 0.0)
		return (dot_product(temp, temp));
	return (0);
}

static t_color	check_color(t_color res)
{
	res.r = (res.r > 255) ? 255 : res.r;
	res.g = (res.g > 255) ? 255 : res.g;
	res.b = (res.b > 255) ? 255 : res.b;
	res.r = (res.r < 0) ? 0 : res.r;
	res.g = (res.g < 0) ? 0 : res.g;
	res.b = (res.b < 0) ? 0 : res.b;
	return (res);
}

static t_color	set_light_color(t_color *light, t_object *obj, float intens)
{
	t_color	res;

	res.r = ((light->r + obj->rgb->r) * intens);
	res.g = ((light->g + obj->rgb->g) * intens);
	res.b = ((light->b + obj->rgb->b) * intens);
	return (res);
}

static t_color	add_light_color(t_color *light, t_object *obj,
	float intens, t_color res)
{
	res.r += ((light->r + obj->rgb->r) * intens);
	res.g += ((light->g + obj->rgb->g) * intens);
	res.b += ((light->b + obj->rgb->b) * intens);
	res = check_color(res);
	return (res);
}

t_color			calculate_light(float t, t_all_objects *all,
	t_ray_data *d, t_object *obj)
{
	t_ray_data	*tmp;
	t_light		*tmp_light;
	t_color		res;
	float		var[3];

	var[0] = g_ambient_light;
	res = set_light_color(all->ambient_lcolor, obj, var[0]);
	if (!make_shadow_ray_data(&tmp))
		return (res);
	tmp_light = all->lights;
	*tmp->ray_o = vector_add(*d->ray_o, vector_multiply(t - BIAS, *d->ray_dir));
	while (tmp_light != 0)
	{
		var[2] = found_first(all, tmp, tmp_light);
		if (var[2] > 0.0)
		{
			var[1] = fabs(calculate_normal(obj, *tmp->ray_dir,
				*tmp->ray_o));
			var[0] += ((tmp_light->brightness * var[1]) / var[2]);
			res = add_light_color(tmp_light->rgb, obj, var[0] * var[1], res);
		}
		tmp_light = tmp_light->next;
	}
	clear_shadow_ray(tmp);
	return (res);
}

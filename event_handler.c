/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/05 19:23:48 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/18 16:32:54 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	ft_image_setup(t_minirt *minirt)
{
	if (minirt->img_ptr)
		mlx_destroy_image(minirt->mlx_ptr, minirt->img_ptr);
	if (!(minirt->img_ptr = mlx_new_image(minirt->mlx_ptr, g_x_res, g_y_res)))
		return (-1);
	return (1);
}

int			ft_window_setup(t_all_objects *all, int save)
{
	if (!save)
	{
		if (!(all->minirt->win_ptr = mlx_new_window(all->minirt->mlx_ptr,
				g_x_res, g_y_res, "Mini_RT_killer")))
			return (0);
		mlx_key_hook(all->minirt->win_ptr, key_hook, all);
		mlx_hook(all->minirt->win_ptr, 17, (1L << X_EVENT), exit_program, all);
	}
	if (ft_image_setup(all->minirt) == -1)
		return (0);
	return (1);
}

int			key_hook(int keycode, t_all_objects *param)
{
	if (keycode == K_AR_L)
		param->cameras = param->cameras->prev;
	else if (keycode == K_AR_R)
		param->cameras = param->cameras->next;
	else if (keycode == K_ESC)
		exit_program(param);
	if (keycode == K_AR_L || keycode == K_AR_R)
	{
		if (ft_image_setup(param->minirt) == -1)
			return (-1);
		render(param, 0);
	}
	return (0);
}

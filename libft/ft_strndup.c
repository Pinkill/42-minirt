/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/16 04:08:14 by user42            #+#    #+#             */
/*   Updated: 2020/08/16 04:08:40 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *src, size_t n)
{
	char	*dest;
	size_t	i;

	i = ft_strlen(src) + 1;
	if (n < i)
	{
		if (!(dest = (char*)malloc(n + 1)))
			return (0);
		ft_memcpy(dest, src, n);
		dest[n] = '\0';
	}
	else
	{
		if (!(dest = (char*)malloc(i + 1)))
			return (0);
		ft_memcpy(dest, src, i);
		dest[i] = '\0';
	}
	return (dest);
}

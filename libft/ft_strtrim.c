/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 21:12:56 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/16 04:25:15 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_checkendset(char *s1, char *set, size_t i)
{
	size_t	j;
	size_t	count;

	j = 0;
	count = 0;
	while (set[j] != '\0')
	{
		if (s1[i] == set[j++])
		{
			i--;
			count++;
			j = 0;
		}
	}
	return (count);
}

size_t			ft_checkset(char *s1, char *set)
{
	size_t	j;
	size_t	i;
	size_t	count;

	count = 0;
	j = 0;
	i = 0;
	if (!s1 || !set)
		return (0);
	while (set[j] != '\0')
	{
		if (s1[i] == set[j])
		{
			i++;
			count += 1;
			j = 0;
		}
		else
			j++;
	}
	return (count);
}

char			*ft_strtrim(char *s1, char *set)
{
	char	*result;
	size_t	ssize;
	size_t	start;
	size_t	end;

	if (s1 == 0 || set == 0)
		return (0);
	start = 0;
	end = 0;
	ssize = ft_strlen(s1);
	start = ft_checkset(s1, set);
	if (start < ssize)
		end = ft_checkendset(s1, set, ssize - 1);
	if (ssize - start - end == 0 ||
		!(result = (char*)malloc(sizeof(char) * (ssize - start - end + 1))))
	{
		free(s1);
		return (0);
	}
	ft_memcpy(result, s1 + start, ssize - start - end);
	result[ssize - start - end] = '\0';
	free(s1);
	if (*result == '\0')
		return (0);
	return (result);
}

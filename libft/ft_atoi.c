/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 10:01:17 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/16 04:20:24 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char **str)
{
	unsigned int		result;
	int					minus;

	result = 0;
	minus = 1;
	while (ft_isspace(**str) == 1)
		*str += 1;
	if (**str == '-' || **str == '+')
	{
		if (**str == '-')
			minus = -1;
		*str += 1;
	}
	while (**str != '\0' && **str >= '0' && **str <= '9')
	{
		result *= 10;
		result += (**str - '0');
		*str += 1;
	}
	return (result * minus);
}

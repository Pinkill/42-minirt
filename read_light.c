/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_light.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/30 19:36:07 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/06 21:18:22 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	ft_default_light(t_light *light)
{
	light->next = 0;
	light->pos = 0;
	light->rgb = 0;
	light->brightness = 0;
}

static int	ft_read_position(char **line, t_light *light)
{
	if (!(light->pos = malloc(sizeof(t_pos))))
		return (0);
	light->pos->x = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	light->pos->y = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	light->pos->z = ft_atof(line);
	return (1);
}

static int	ft_read_color(char **line, t_light *light)
{
	if (!(light->rgb = malloc(sizeof(t_color))))
		return (0);
	light->rgb->r = ft_atoi((const char **)line);
	if (!(ft_check_comma(line)))
		return (0);
	if (light->rgb->r < 0 || light->rgb->r > 255)
		return (0);
	light->rgb->g = ft_atoi((const char **)line);
	if (!(ft_check_comma(line)))
		return (0);
	if (light->rgb->g < 0 || light->rgb->g > 255)
		return (0);
	light->rgb->b = ft_atoi((const char **)line);
	if (light->rgb->b < 0 || light->rgb->b > 255)
		return (0);
	return (1);
}

int			read_light(char *line, t_all_objects *all)
{
	t_light	*new;

	if (!(new = malloc(sizeof(t_light))))
		return (0);
	ft_default_light(new);
	ft_light_add(&all->lights, new);
	new->next = 0;
	if (!(ft_read_position(&line, new)))
		return (0);
	new->brightness = ft_atof(&line);
	if (new->brightness > 1.0 || new->brightness < 0.0)
		return (0);
	if (!(ft_read_color(&line, new)))
		return (0);
	return (1);
}

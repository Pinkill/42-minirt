/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_square.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/04 20:17:43 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/21 18:32:24 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	free_new(t_object *new)
{
	if (new->pos)
		free(new->pos);
	if (new->orien_vec)
		free(new->orien_vec);
	if (new->rgb)
		free(new->rgb);
	free(new);
	return (0);
}

static void	calc_sqr_sides(t_object *sqr)
{
	float	*sqr_to_world;

	sqr->sqr_a = malloc(sizeof(t_pos));
	sqr->sqr_b = malloc(sizeof(t_pos));
	sqr->sqr_c = malloc(sizeof(t_pos));
	sqr_to_world = rotate_object(sqr, 0);
	sqr->sqr_a->x = sqr->side / 2;
	sqr->sqr_a->y = sqr->side / 2;
	sqr->sqr_a->z = 0;
	sqr->sqr_b->x = -sqr->side / 2;
	sqr->sqr_b->y = -sqr->side / 2;
	sqr->sqr_b->z = 0;
	sqr->sqr_c->x = -sqr->side / 2;
	sqr->sqr_c->y = sqr->side / 2;
	sqr->sqr_c->z = 0;
	vector_matrix_multiply(sqr->sqr_a, sqr_to_world);
	vector_matrix_multiply(sqr->sqr_b, sqr_to_world);
	vector_matrix_multiply(sqr->sqr_c, sqr_to_world);
	free(sqr_to_world);
}

int			read_square(char *line, t_all_objects *all)
{
	t_object	*new;

	if (!(new = malloc(sizeof(t_object))))
		return (0);
	ft_default_object(new);
	new->id = 3;
	new->next = 0;
	if (!(ft_read_elposition(&line, new)))
		return (free_new(new));
	if (!(ft_read_elo_vec(&line, new)))
		return (free_new(new));
	new->side = ft_atof(&line);
	if (new->side < 0.0)
		new->side = 0.0;
	if (!(ft_read_elcolor(&line, new)))
		return (free_new(new));
	calc_sqr_sides(new);
	ft_object_add(&all->objects, new);
	return (1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shadow_ray.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/04 19:58:48 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/05 22:21:52 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int		make_shadow_ray_data(t_ray_data **data)
{
	if (!(*data = malloc(sizeof(t_ray_data))))
		return (0);
	if (!((*data)->ray_dir = malloc(sizeof(t_pos))))
	{
		clear_shadow_ray(*data);
		return (0);
	}
	if (!((*data)->ray_o = malloc(sizeof(t_pos))))
	{
		clear_shadow_ray(*data);
		return (0);
	}
	(*data)->buffer = 0;
	return (1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launcher.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 17:05:34 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/18 17:32:38 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void		draw_pixel(t_ray_data *data, t_all_objects *all,
	float *cam_to_world, int i[2])
{
	t_object	*first;
	float		t;

	first = 0;
	data->ray_dir->y = (1 - 2 * ((i[0] + 0.5) / g_y_res) *
		tan(all->cameras->fov * 0.5 * M_PI / 180.0));
	data->ray_dir->x = (2 * ((i[1] + 0.5) / g_x_res) - 1) *
		tan(all->cameras->fov * 0.5 * M_PI / 180.0)
		* ((float)g_x_res / g_y_res);
	data->ray_dir->z = 1;
	direction_matrix_multiply(data->ray_dir, cam_to_world);
	*data->ray_dir = normalize(*data->ray_dir);
	t = intersect_first(all->objects, data, &first, INF);
	if (first != 0)
		set_pixel(data->buffer, calculate_light(t, all, data, first), i);
	else
		set_pixel_ambient(data->buffer, i, all);
}

static char		*send_buffer(t_minirt *minirt)
{
	int		bpp;
	int		size_line;
	int		endian;
	char	*buffer;

	buffer = mlx_get_data_addr(minirt->img_ptr, &bpp, &size_line, &endian);
	return (buffer);
}

int				render(t_all_objects *all, int save)
{
	int				i[2];
	t_ray_data		*data;
	float			*cam_to_world;

	if (!all->ambient_lcolor)
		return (throw_error(all, 9, 0));
	data = malloc(sizeof(t_ray_data));
	cam_to_world = calc_cam_origin(all->cameras, data);
	i[0] = -1;
	data->buffer = send_buffer(all->minirt);
	while (++i[0] < g_y_res)
	{
		i[1] = -1;
		while (++i[1] < g_x_res)
			draw_pixel(data, all, cam_to_world, i);
	}
	free(cam_to_world);
	if (save == 1)
		store_picture(data->buffer, 32, g_y_res * g_x_res * 4, all);
	mlx_clear_window(all->minirt->mlx_ptr, all->minirt->win_ptr);
	mlx_put_image_to_window(all->minirt->mlx_ptr, all->minirt->win_ptr,
		all->minirt->img_ptr, 0, 0);
	mlx_loop(all->minirt->mlx_ptr);
	return (1);
}

static int		launcher(char *file, int save)
{
	int				fd;
	int				res;
	t_all_objects	*all;

	if (is_file_empty(file) || (fd = open(file, O_RDONLY)) < 1)
		return (throw_error(0, 9, 0));
	if (!(all = malloc(sizeof(t_all_objects))))
		return (throw_error(0, 14, fd));
	ft_default_all(all);
	if (!(all->minirt = malloc(sizeof(t_minirt))))
		return (throw_error(all, 14, fd));
	ft_default_minirt(all->minirt);
	if (!(all->minirt->mlx_ptr = mlx_init()))
		return (throw_error(all, 14, fd));
	if ((res = read_rt(all, fd)) == -1)
		return (throw_error(all, 9, fd));
	if (res == -2)
		return (throw_error(all, 22, fd));
	if (!ft_window_setup(all, save))
		return (throw_error(all, 14, fd));
	close(fd);
	render(all, save);
	return (0);
}

int				main(int argc, char **argv)
{
	int	len;

	if (argc > 1 && argc < 4)
	{
		len = ft_strlen(argv[1]);
		g_x_res = 0;
		g_y_res = 0;
		if (len > 2 && argv[1][len - 1] == 't' && argv[1][len - 2] == 'r'
			&& argv[1][len - 3] == '.')
		{
			if (argc > 2 && ft_strncmp(argv[2], "--save", 6) == 0)
				launcher(argv[1], 1);
			else if (argc == 2)
				launcher(argv[1], 0);
		}
		else
			ft_putstr_fd("Error:\n\tWrong file\n", 1);
	}
	return (0);
}

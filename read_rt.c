/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_rt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 17:15:49 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/15 18:37:00 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	ft_elements(char *str)
{
	if (str[0] == 's' && str[1] == 'p')
		return (1);
	else if (str[0] == 'p' && str[1] == 'l')
		return (2);
	else if (str[0] == 's' && str[1] == 'q')
		return (3);
	else if (str[0] == 'c' && str[1] == 'y')
		return (4);
	else if (str[0] == 't' && str[1] == 'r')
		return (5);
	else if (str[0] == 'c')
		return (6);
	else if (str[0] == 'l')
		return (7);
	return (0);
}

static int	ft_read_element(char *str, t_all_objects *all, int ret)
{
	if (ret == 1)
		return (read_sphere(str, all));
	if (ret == 2)
		return (read_plane(str, all));
	if (ret == 3)
		return (read_square(str, all));
	if (ret == 4)
		return (read_cylinder(str, all));
	if (ret == 5)
		return (read_triangle(str, all));
	return (0);
}

static int	ft_enviroment(char *str, t_all_objects *all)
{
	int	ret;

	ret = ft_elements(str);
	if (*str == 'R')
		return (read_resolution(str + 1, all));
	else if (*str == 'A')
		return (read_ambientl(str + 1, all));
	else if (ret == 6)
		return (read_camera(str + 1, all));
	else if (ret == 7)
		return (read_light(str + 1, all));
	else if (ret > 0 && ret < 6)
		return (ft_read_element(str + 2, all, ret));
	else
		return (0);
}

static int	read_element(char *line, t_all_objects *all)
{
	if (line[0] != '\0' && line[1] != '\0')
	{
		if (!ft_enviroment(line, all))
			return (0);
	}
	return (1);
}

int			read_rt(t_all_objects *all, int fd)
{
	int		ret;
	char	*line;

	ret = 0;
	line = 0;
	while ((ret = get_next_line(fd, &line)) > -1)
	{
		if (ft_checkset(line, " \n\t\f\v\r"))
			line = ft_strtrim(line, " \n\t\f\v\r");
		if (!line)
			continue ;
		else if (read_element(line, all) == 0)
			return (free_line(&line, -2));
		if (ret == 0)
			return (free_line(&line, 0));
		free(line);
	}
	if (line)
		free(line);
	return (ret);
}

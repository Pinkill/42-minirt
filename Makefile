# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: user42 <user42@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/07 15:39:40 by mknezevi          #+#    #+#              #
#    Updated: 2020/08/18 16:24:10 by user42           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = launcher.c get_next_line.c ft_atof.c read_light.c rotations_obj.c \
read_ambientl.c read_camera.c add_back_default.c throw_error.c \
ft_check_comma.c matrix_math.c rotations_camera.c ft_read_elvalues.c \
draw_image.c normals.c light_handling.c minirt_utils.c read_square.c \
read_elements.c read_rt.c vector_math.c intersects_ad.c vector_math_ad.c \
intersects.c shadow_ray.c read_cylinder.c event_handler.c clear_all.c

BONUSES =

SRCS = ${addprefix ${PRE}, ${SRC}}

OBJS = ${SRCS:.c=.o}

BONUS_OBJS = ${BONUSES:.c=.o}

PRE = ./

HEAD = ./

NAME = miniRT

GCC = gcc

CFLAG = -Llibs -Iincludes -Wall -Wextra -Werror

MINILIBX_FLAG_MAC = -framework OpenGL -framework AppKit

MINILIBX_FLAG_LIN = -lXext -lX11

SUBSYSTEM_LIBS = -lmlx -lft -lm

all:	lib_build $(NAME)

all_lin:	lib_build_lin $(NAME)_lin

%.o: %.c
	${GCC} ${CFLAG} -c -I ${HEAD} $< -o ${<:.c=.o}

$(NAME):
	${GCC} ${CFLAG} ${SRCS} ${SUBSYSTEM_LIBS} ${MINILIBX_FLAG_MAC} -o ${NAME}

$(NAME)_lin:
	${GCC} ${CFLAG} ${SRCS} ${SUBSYSTEM_LIBS} ${MINILIBX_FLAG_LIN} -o ${NAME}

clean:
	rm -f ${OBJS}
	rm -f ${BONUS_OBJS}
	rm -f ${NAME}

fclean:	lib_fclean clean

re: 	fclean all

re_lin: fclean all_lin

lib_build:
	cd ./minilibx_opengl_20191021 && $(MAKE) all && mv ./libmlx.a ../libs/
	cd ./libft && $(MAKE) all && mv ./libft.a ../libs/

lib_build_lin:
	cd ./minilibx-linux-master && $(MAKE) all && mv ./libmlx.a ../libs/
	cd ./libft && $(MAKE) all && mv ./libft.a ../libs/

lib_fclean:
	cd ./minilibx_opengl_20191021 && $(MAKE) clean
	cd ./minilibx-linux-master && $(MAKE) clean
	cd ./libft && $(MAKE) fclean
	rm -f ./libs/*

.PHONY:		all bonus clean fclean re

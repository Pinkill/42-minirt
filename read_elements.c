/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_elements.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/01 00:54:56 by matt              #+#    #+#             */
/*   Updated: 2020/08/06 21:14:33 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	ft_read_point_pos(char **line, t_pos *pos)
{
	pos->x = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	pos->y = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	pos->z = ft_atof(line);
	return (1);
}

int			read_triangle(char *line, t_all_objects *all)
{
	t_object	*new;

	if (!(new = malloc(sizeof(t_object))))
		return (0);
	ft_default_object(new);
	ft_object_add(&all->objects, new);
	new->id = 5;
	new->next = 0;
	if (!(new->triangle = malloc(sizeof(t_triangle))))
		return (0);
	if (!(new->triangle->pos_0 = malloc(sizeof(t_pos))))
		return (0);
	if (!(ft_read_point_pos(&line, new->triangle->pos_0)))
		return (0);
	if (!(new->triangle->pos_1 = malloc(sizeof(t_pos))))
		return (0);
	if (!(ft_read_point_pos(&line, new->triangle->pos_1)))
		return (0);
	if (!(new->triangle->pos_2 = malloc(sizeof(t_pos))))
		return (0);
	if (!(ft_read_point_pos(&line, new->triangle->pos_2)))
		return (0);
	if (!(ft_read_elcolor(&line, new)))
		return (0);
	return (1);
}

int			read_plane(char *line, t_all_objects *all)
{
	t_object	*new;

	if (!(new = malloc(sizeof(t_object))))
		return (0);
	ft_default_object(new);
	ft_object_add(&all->objects, new);
	new->id = 2;
	new->next = 0;
	if (!(ft_read_elposition(&line, new)))
		return (0);
	if (!(ft_read_elo_vec(&line, new)))
		return (0);
	if (!(ft_read_elcolor(&line, new)))
		return (0);
	return (1);
}

int			read_sphere(char *line, t_all_objects *all)
{
	t_object	*new;

	if (!(new = malloc(sizeof(t_object))))
		return (0);
	ft_default_object(new);
	ft_object_add(&all->objects, new);
	new->id = 1;
	new->next = 0;
	if (!(ft_read_elposition(&line, new)))
		return (0);
	new->diameter = ft_atof(&line);
	if (new->diameter < 0.0)
		new->diameter = 0.0;
	if (!(ft_read_elcolor(&line, new)))
		return (0);
	return (1);
}

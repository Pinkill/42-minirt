/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 17:14:33 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/18 17:53:52 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stdlib.h>
# include <unistd.h>

void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memset(void *b, int c, size_t len);

char			*ft_strndup(const char *src, size_t n);

int				ft_atoi(const char **str);
int				ft_isspace(char c);
size_t			ft_strlen(const char *str);
int				ft_strncmp(const char *s1, const char *s2, size_t n);

void			ft_putstr_fd(char *s, int fd);
char			*ft_strjoin(char *s1, char *s2);
size_t			ft_checkset(char *s1, char *set);
char			*ft_strtrim(char *s1, char *set);

#endif

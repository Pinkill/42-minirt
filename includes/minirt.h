/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/10 13:44:42 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/18 16:30:46 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H
# define EPSILON 0.00001
# define BIAS 0.001
# define INF 0x7ffffff
# define SPHERE 1
# define PLANE 2
# define SQUARE 3
# define CYLINDER 4
# define TRIANGLE 5

# include <math.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <fcntl.h>
# include <stdio.h>
# include "get_next_line.h"
# include "libft.h"
# if defined(__APPLE__)
#  include <key_macos.h>
#  include "../minilibx_opengl_20191021/mlx.h"
# else
#  define __APPLE__ 0
#  include <key_linux.h>
#  include "../minilibx-linux-master/mlx.h"
# endif

typedef struct	s_position
{
	float	x;
	float	y;
	float	z;
}				t_pos;

typedef struct	s_color
{
	int		r;
	int		g;
	int		b;
}				t_color;

typedef struct	s_triangle
{
	t_pos		*pos_0;
	t_pos		*pos_1;
	t_pos		*pos_2;
}				t_triangle;

typedef struct	s_minirt
{
	void	*mlx_ptr;
	void	*win_ptr;
	void	*img_ptr;
}				t_minirt;

typedef struct	s_camera
{
	t_pos			*pos;
	t_pos			*orien_vec;
	int				id;
	int				fov;
	struct s_camera	*next;
	struct s_camera	*prev;
}				t_camera;

typedef struct	s_light
{
	t_pos			*pos;
	t_color			*rgb;
	float			brightness;
	struct s_light	*next;
}				t_light;

typedef	struct	s_object
{
	int				id;
	t_pos			*pos;
	t_pos			*orien_vec;
	t_color			*rgb;
	float			diameter;
	float			side;
	float			height;
	t_triangle		*triangle;
	t_pos			*top;
	t_pos			*sqr_a;
	t_pos			*sqr_b;
	t_pos			*sqr_c;
	struct s_object	*next;
}				t_object;

typedef struct	s_ray_data
{
	t_pos	*ray_dir;
	t_pos	*ray_o;
	char	*buffer;
}				t_ray_data;

typedef struct	s_all_objects
{
	t_minirt	*minirt;
	t_camera	*cameras;
	t_object	*objects;
	t_light		*lights;
	t_color		*ambient_lcolor;
}				t_all_objects;

int					g_x_res;
int					g_y_res;
float				g_ambient_light;

int				render(t_all_objects *all, int save);
int				read_rt(t_all_objects *all, int fd);
void			ft_default_all(t_all_objects *all);
void			ft_default_object(t_object *object);
int				read_resolution(char *line, t_all_objects *all);
int				read_ambientl(char *line, t_all_objects *all);
int				read_camera(char *line, t_all_objects *all);
int				read_light(char *line, t_all_objects *all);
int				read_sphere(char *line, t_all_objects *all);
int				read_plane(char *line, t_all_objects *all);
int				read_square(char *line, t_all_objects *all);
int				read_cylinder(char *line, t_all_objects *all);
int				read_triangle(char *line, t_all_objects *all);
float			ft_atof(char **str);
int				is_file_empty(char *file);
int				throw_error(t_all_objects *all, int errnum, int fd);
void			ft_object_add(t_object **alst, t_object *new);
void			ft_camera_add(t_camera **alst, t_camera *new);
void			ft_light_add(t_light **alst, t_light *new);
int				ft_check_comma(char **line);
int				free_line(char **line, int ret);
int				ft_read_elcolor(char **line, t_object *object);
int				ft_read_elposition(char **line, t_object *object);
int				ft_read_elo_vec(char **line, t_object *object);
t_pos			cross_product(t_pos v1, t_pos v2);
float			dot_product(t_pos v1, t_pos v2);
t_pos			vector_multiply(float n, t_pos v);
t_pos			vector_add(t_pos v1, t_pos v2);
t_pos			vector_subtract(t_pos v1, t_pos v2);
float			*make_matrix(void);
float			*matrix_multiply(float *m1, float *m2);
void			vector_matrix_multiply(t_pos *v1, float m1[16]);
void			direction_matrix_multiply(t_pos *v1, float m1[16]);
t_pos			normalize(t_pos v);
float			points_distance(t_pos p1, t_pos p2);
float			intersect_sphere(t_object *sp, t_pos *ray_o, t_pos *ray_d);
float			intersect_cylinder(t_object *cy, t_pos *ray_o, t_pos *ray_dir);
float			plane_intersect(t_object *pl, t_pos *ray_o, t_pos *ray_dir);
float			intersect_square(t_object *sq, t_pos *ray_o, t_pos *ray_dir);
float			intersect_triangle(t_object *tr, t_pos *ray_o, t_pos *ray_dir);
float			intersect_first(t_object *objs, t_ray_data *data,
					t_object **first, float tmin);
void			set_pixel(char *buffer, t_color rgb, int i[2]);
void			set_pixel_ambient(char *buf, int i[2], t_all_objects *all);
int				store_picture(char *buffer, int bpp, int n, t_all_objects *all);
float			calculate_normal(t_object *object, t_pos to_light, t_pos point);
t_color			calculate_light(float t, t_all_objects *all,
					t_ray_data *data, t_object *obj);
int				exit_program(t_all_objects *all);
int				make_shadow_ray_data(t_ray_data **data);
void			free_shadow_ray(t_ray_data *data);
float			*rotate_object(t_object *obj, int translate);
float			*rotate_camera(t_camera *cam);
int				ft_window_setup(t_all_objects *all, int save);
int				key_hook(int keycode, t_all_objects *param);
void			ft_default_minirt(t_minirt *minirt);
float			*calc_cam_origin(t_camera *cam, t_ray_data *data);
int				clear_all(t_all_objects *all);
void			clear_triangle(t_object *obj);
void			clear_shadow_ray(t_ray_data *data);

#endif

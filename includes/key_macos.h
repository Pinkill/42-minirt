/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_macos.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/13 20:28:48 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/15 22:12:12 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEY_MACOS_H
# define KEY_MACOS_H

# define K_AR_L 123
# define K_AR_R 124
# define K_ESC 53
# define MAX_X_RES 2560
# define MAX_Y_RES 1440
# define X_EVENT 0

#endif

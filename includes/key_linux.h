/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_linux.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/13 20:31:33 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/15 22:12:58 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEY_LINUX_H
# define KEY_LINUX_H

# define K_AR_L 65361
# define K_AR_R 65363
# define K_ESC 65307
# define MAX_X_RES 0
# define MAX_Y_RES 0
# define X_EVENT 17

#endif

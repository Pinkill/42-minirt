/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:39:04 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/11 19:43:04 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 1000
# endif

# include <stdlib.h>
# include <unistd.h>

typedef struct	s_file
{
	int		fd;
	char	*buf;
	int		res;
	int		chars;
}				t_file;

int				get_next_line(int fd, char **line);
size_t			ft_strlen(const char *str);
char			*ft_strjoin(char *s1, char *s2);

void			*ft_memcpy(void *dst, const void *src, size_t n);
char			*ft_strndup(const char *src, size_t n);

#endif

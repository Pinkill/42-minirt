/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_math_ad.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 22:21:06 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/05 19:22:50 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

t_pos	normalize(t_pos v)
{
	float w;

	w = v.x * v.x + v.y * v.y + v.z * v.z;
	if (w > 0)
	{
		w = 1 / sqrt(w);
		v.x *= w;
		v.y *= w;
		v.z *= w;
	}
	return (v);
}

float	points_distance(t_pos p1, t_pos p2)
{
	return (sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2)
		+ pow(p2.z - p1.z, 2)));
}

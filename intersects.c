/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersects.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 22:44:16 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/06 22:32:07 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

float			plane_intersect(t_object *pl, t_pos *ray_o, t_pos *ray_dir)
{
	float		d;
	float		t;
	t_pos		p010;

	d = dot_product(normalize(*pl->orien_vec), *ray_dir);
	if (d < EPSILON && d > -EPSILON)
		return (0);
	else
	{
		p010.x = pl->pos->x - ray_o->x;
		p010.y = pl->pos->y - ray_o->y;
		p010.z = pl->pos->z - ray_o->z;
		t = dot_product(p010, normalize(*pl->orien_vec)) / d;
		if (t >= 0)
			return (t);
	}
	return (0);
}

float			intersect_square(t_object *sq, t_pos *ray_o, t_pos *ray_dir)
{
	float		s[5];
	t_pos		vec[3];
	t_pos		point;
	t_pos		normal;

	vec[0] = vector_subtract(*sq->sqr_a, *sq->sqr_c);
	vec[1] = vector_subtract(*sq->sqr_c, *sq->sqr_b);
	normal = cross_product(vec[0], vec[1]);
	s[2] = dot_product(vector_subtract(*sq->pos, *ray_o), normal)
		/ dot_product(*ray_dir, normal);
	if (s[2] <= 0)
		return (0);
	vec[2] = vector_add(*ray_o, vector_multiply(s[2], *ray_dir));
	point = vector_subtract(vec[2], *sq->pos);
	s[3] = dot_product(vec[0], vec[0]);
	s[4] = dot_product(vec[1], vec[1]);
	s[0] = dot_product(point, vec[0]);
	s[1] = dot_product(point, vec[1]);
	if ((s[0] < s[3] && s[0] > 0) && (s[1] < s[4] && s[1] > 0))
		return (s[2]);
	return (0);
}

float			intersect_triangle(t_object *tr, t_pos *ray_o, t_pos *ray_dir)
{
	t_pos		vec[6];
	float		s[5];

	vec[0] = vector_subtract(*tr->triangle->pos_1, *tr->triangle->pos_0);
	vec[1] = vector_subtract(*tr->triangle->pos_2, *tr->triangle->pos_0);
	vec[5] = cross_product(vec[1], vec[0]);
	s[3] = dot_product(vector_subtract(*tr->triangle->pos_0, *ray_o), vec[5])
		/ dot_product(*ray_dir, vec[5]);
	if (s[3] <= 0)
		return (0);
	vec[2] = cross_product(*ray_dir, vec[0]);
	s[4] = dot_product(vec[1], vec[2]);
	if (s[4] > -EPSILON && s[4] < EPSILON)
		return (0);
	s[0] = 1.0 / s[4];
	vec[3] = vector_subtract(*ray_o, *tr->triangle->pos_0);
	s[1] = dot_product(vec[3], vec[2]) * s[0];
	if (s[1] < 0.0 || s[1] > 1.0)
		return (0);
	vec[4] = cross_product(vec[3], vec[1]);
	s[2] = dot_product(*ray_dir, vec[4]) * s[0];
	if (s[2] < 0.0 || s[1] + s[2] > 1.0)
		return (0);
	s[3] = dot_product(vec[0], vec[4]) * s[0];
	return (s[3]);
}

static float	intersect_object(t_object *objs, t_pos *ray_o, t_pos *ray_dir)
{
	float	t;

	t = 0;
	if (objs->id == 1)
		t = intersect_sphere(objs, ray_o, ray_dir);
	else if (objs->id == 2)
		t = plane_intersect(objs, ray_o, ray_dir);
	else if (objs->id == 3)
		t = intersect_square(objs, ray_o, ray_dir);
	else if (objs->id == 4)
		t = intersect_cylinder(objs, ray_o, ray_dir);
	else if (objs->id == 5)
		t = intersect_triangle(objs, ray_o, ray_dir);
	return (t);
}

float			intersect_first(t_object *objs,
	t_ray_data *data, t_object **first, float tmin)
{
	t_object	*tmp_obj;
	float		t;

	tmp_obj = objs;
	t = INF;
	while (tmp_obj != 0)
	{
		t = intersect_object(tmp_obj, data->ray_o, data->ray_dir);
		if (t > 0 && t < tmin)
		{
			tmin = t;
			*first = tmp_obj;
		}
		tmp_obj = tmp_obj->next;
	}
	return (tmin);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_math.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 22:13:54 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/05 22:35:04 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

float	*make_matrix(void)
{
	float	*mat;

	mat = (float *)malloc(sizeof(float) * 16);
	mat[0] = 1;
	mat[1] = 0;
	mat[2] = 0;
	mat[3] = 0;
	mat[4] = 0;
	mat[5] = 1;
	mat[6] = 0;
	mat[7] = 0;
	mat[8] = 0;
	mat[9] = 0;
	mat[10] = 1;
	mat[11] = 0;
	mat[12] = 0;
	mat[13] = 0;
	mat[14] = 0;
	mat[15] = 1;
	return (mat);
}

float	*matrix_multiply(float *m1, float *m2)
{
	float *res;

	res = (float *)malloc(sizeof(float) * 16);
	res[0] = m1[0] * m2[0] + m1[4] * m2[1] + m1[8] * m2[2] + m1[12] * m2[3];
	res[1] = m1[1] * m2[0] + m1[5] * m2[1] + m1[9] * m2[2] + m1[13] * m2[3];
	res[2] = m1[2] * m2[0] + m1[6] * m2[1] + m1[10] * m2[2] + m1[14] * m2[3];
	res[3] = m1[3] * m2[0] + m1[7] * m2[1] + m1[11] * m2[2] + m1[15] * m2[3];
	res[4] = m1[0] * m2[4] + m1[4] * m2[5] + m1[8] * m2[6] + m1[12] * m2[7];
	res[5] = m1[1] * m2[4] + m1[5] * m2[5] + m1[9] * m2[6] + m1[13] * m2[7];
	res[6] = m1[2] * m2[4] + m1[6] * m2[5] + m1[10] * m2[6] + m1[14] * m2[7];
	res[7] = m1[3] * m2[4] + m1[7] * m2[5] + m1[11] * m2[6] + m1[15] * m2[7];
	res[8] = m1[0] * m2[8] + m1[4] * m2[9] + m1[8] * m2[10] + m1[12] * m2[11];
	res[9] = m1[1] * m2[8] + m1[5] * m2[9] + m1[9] * m2[10] + m1[13] * m2[11];
	res[10] = m1[2] * m2[8] + m1[6] * m2[9] + m1[10] * m2[10] + m1[14] * m2[11];
	res[11] = m1[3] * m2[8] + m1[7] * m2[9] + m1[11] * m2[10] + m1[15] * m2[11];
	res[12] = m1[0] * m2[12] + m1[4] * m2[13]
		+ m1[8] * m2[14] + m1[12] * m2[15];
	res[13] = m1[1] * m2[12] + m1[5] * m2[13]
		+ m1[9] * m2[14] + m1[13] * m2[15];
	res[14] = m1[2] * m2[12] + m1[6] * m2[13]
		+ m1[10] * m2[14] + m1[14] * m2[15];
	res[15] = m1[3] * m2[12] + m1[7] * m2[13]
		+ m1[11] * m2[14] + m1[15] * m2[15];
	free(m1);
	return (res);
}

void	vector_matrix_multiply(t_pos *v1, float m1[16])
{
	t_pos		res;
	float		w;

	res.x = v1->x * m1[0] + v1->y * m1[1] + v1->z * m1[2] + m1[3];
	res.y = v1->x * m1[4] + v1->y * m1[5] + v1->z * m1[6] + m1[7];
	res.z = v1->x * m1[8] + v1->y * m1[9] + v1->z * m1[10] + m1[11];
	w = v1->x * m1[12] + v1->y * m1[13] + v1->z * m1[14] + m1[15];
	v1->x = res.x / w;
	v1->y = res.y / w;
	v1->z = res.z / w;
}

void	direction_matrix_multiply(t_pos *v1, float m1[16])
{
	t_pos	res;

	res.x = v1->x * m1[0] + v1->y * m1[1] + v1->z * m1[2];
	res.y = v1->x * m1[4] + v1->y * m1[5] + v1->z * m1[6];
	res.z = v1->x * m1[8] + v1->y * m1[9] + v1->z * m1[10];
	v1->x = res.x;
	v1->y = res.y;
	v1->z = res.z;
}

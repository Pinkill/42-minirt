/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_cylinder.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/04 23:18:26 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/21 18:32:11 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int	free_new(t_object *new)
{
	if (new->pos)
		free(new->pos);
	if (new->orien_vec)
		free(new->orien_vec);
	if (new->rgb)
		free(new->rgb);
	free(new);
	return (0);
}

static void	calc_cyl_top(t_object *cy)
{
	float		*cylinder_to_world;

	cy->top = malloc(sizeof(t_pos));
	cy->top->x = 0;
	cy->top->y = cy->height;
	cy->top->z = 0;
	cylinder_to_world = rotate_object(cy, 1);
	vector_matrix_multiply(cy->top, cylinder_to_world);
	free(cylinder_to_world);
}

int			read_cylinder(char *line, t_all_objects *all)
{
	t_object	*new;

	if (!(new = malloc(sizeof(t_object))))
		return (0);
	ft_default_object(new);
	new->id = 4;
	new->next = 0;
	if (!(ft_read_elposition(&line, new)))
		return (free_new(new));
	if (!(ft_read_elo_vec(&line, new)))
		return (free_new(new));
	new->diameter = ft_atof(&line);
	new->height = ft_atof(&line);
	if (!(ft_read_elcolor(&line, new)))
		return (free_new(new));
	if (new->height < 0 || new->diameter <= 0)
		return (free_new(new));
	calc_cyl_top(new);
	ft_object_add(&all->objects, new);
	return (1);
}

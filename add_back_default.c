/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_back_default.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 20:14:17 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/10 19:14:04 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

void	ft_object_add(t_object **alst, t_object *new)
{
	t_object	*tmp;

	if (*alst == 0)
	{
		*alst = new;
		return ;
	}
	tmp = *alst;
	while (tmp->next != 0)
		tmp = tmp->next;
	tmp->next = new;
}

void	ft_light_add(t_light **alst, t_light *new)
{
	t_light	*tmp;

	if (*alst == 0)
	{
		*alst = new;
		return ;
	}
	tmp = *alst;
	while (tmp->next != 0)
		tmp = tmp->next;
	tmp->next = new;
}

void	ft_camera_add(t_camera **alst, t_camera *new)
{
	t_camera	*tmp;

	if (*alst == 0)
	{
		*alst = new;
		return ;
	}
	tmp = *alst;
	while (tmp->next->id != (*alst)->id)
		tmp = tmp->next;
	tmp->next = new;
	new->prev = tmp;
	new->next = (*alst);
	(*alst)->prev = new;
}

void	ft_default_all(t_all_objects *all)
{
	all->cameras = 0;
	all->lights = 0;
	all->minirt = 0;
	all->objects = 0;
	all->ambient_lcolor = 0;
}

void	ft_default_object(t_object *object)
{
	object->next = 0;
	object->orien_vec = 0;
	object->pos = 0;
	object->rgb = 0;
	object->sqr_a = 0;
	object->sqr_b = 0;
	object->sqr_c = 0;
	object->top = 0;
	object->triangle = 0;
	object->diameter = 0;
	object->height = 0;
	object->id = 0;
	object->side = 0;
}

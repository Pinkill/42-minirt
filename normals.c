/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 22:44:59 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/14 15:46:50 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static float	sphere_normal(t_object *sphere, t_pos to_light, t_pos point)
{
	float	normal;

	normal = dot_product(normalize(vector_subtract(*sphere->pos, point)),
		to_light);
	return (normal);
}

static float	triangle_normal(t_object *tr, t_pos to_light)
{
	t_pos	s1;
	t_pos	s2;
	t_pos	norm;
	float	normal;

	s2 = vector_subtract(*tr->triangle->pos_1, *tr->triangle->pos_0);
	s1 = vector_subtract(*tr->triangle->pos_2, *tr->triangle->pos_0);
	norm = normalize(cross_product(s1, s2));
	normal = dot_product(norm, to_light);
	return (normal);
}

static float	square_normal(t_object *square, t_pos to_light)
{
	t_pos	s1;
	t_pos	s2;
	t_pos	norm;
	float	normal;

	s1 = vector_subtract(*square->sqr_a, *square->sqr_c);
	s2 = vector_subtract(*square->sqr_c, *square->sqr_b);
	norm = normalize(cross_product(s1, s2));
	normal = dot_product(norm, to_light);
	return (normal);
}

static float	cylinder_normal(t_object *cy, t_pos to_light, t_pos point)
{
	t_pos	norm;
	t_pos	temp;
	t_pos	tmp;
	float	s[2];
	float	normal;

	norm = normalize(vector_subtract(*cy->top, *cy->pos));
	s[0] = dot_product(vector_subtract(*cy->top, point), norm);
	s[1] = dot_product(norm, norm);
	temp = vector_add(point, vector_multiply((s[0] / s[1]), norm));
	tmp = vector_subtract(temp, point);
	temp = vector_subtract(*cy->top, tmp);
	norm = normalize(vector_subtract(temp, point));
	normal = dot_product(norm, to_light);
	return (normal);
}

float			calculate_normal(t_object *object, t_pos to_light, t_pos point)
{
	float	normal;
	t_pos	rev;

	normal = 0;
	rev = vector_multiply(-1.0, to_light);
	if (object->id == 1)
		normal = sphere_normal(object, rev, point);
	else if (object->id == 2)
		normal = dot_product(normalize(*object->orien_vec), rev);
	else if (object->id == 3)
		normal = square_normal(object, rev);
	else if (object->id == 4)
		normal = cylinder_normal(object, rev, point);
	else if (object->id == 5)
		normal = triangle_normal(object, rev);
	return (normal);
}

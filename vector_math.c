/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_math.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 22:10:54 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/05 19:22:46 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

t_pos	cross_product(t_pos v1, t_pos v2)
{
	t_pos	x;

	x.x = v1.y * v2.z - v1.z * v2.y;
	x.y = v1.z * v2.x - v1.x * v2.z;
	x.z = v1.x * v2.y - v1.y * v2.x;
	return (x);
}

float	dot_product(t_pos v1, t_pos v2)
{
	float	res;

	res = v1.x * v2.x;
	res += v1.y * v2.y;
	res += v1.z * v2.z;
	return (res);
}

t_pos	vector_multiply(float n, t_pos v)
{
	v.x *= n;
	v.y *= n;
	v.z *= n;
	return (v);
}

t_pos	vector_add(t_pos v1, t_pos v2)
{
	v1.x += v2.x;
	v1.y += v2.y;
	v1.z += v2.z;
	return (v1);
}

t_pos	vector_subtract(t_pos v1, t_pos v2)
{
	v1.x -= v2.x;
	v1.y -= v2.y;
	v1.z -= v2.z;
	return (v1);
}

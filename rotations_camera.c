/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotations_camera.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/04 22:39:47 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/14 15:25:27 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static float	*create_rotation_x(t_camera *cam)
{
	float	*mat;

	mat = make_matrix();
	mat[5] = cos(cam->orien_vec->z);
	mat[6] = -sin(cam->orien_vec->z);
	mat[9] = sin(cam->orien_vec->z);
	mat[10] = cos(cam->orien_vec->z);
	return (mat);
}

static float	*create_rotation_y(t_camera *cam)
{
	float	*mat;

	mat = make_matrix();
	mat[0] = cos(cam->orien_vec->y);
	mat[2] = sin(cam->orien_vec->y);
	mat[8] = -sin(cam->orien_vec->y);
	mat[10] = cos(cam->orien_vec->y);
	return (mat);
}

static float	*create_rotation_z(t_camera *cam)
{
	float	*mat;

	mat = make_matrix();
	mat[0] = cos(cam->orien_vec->x);
	mat[1] = -sin(cam->orien_vec->x);
	mat[4] = sin(cam->orien_vec->x);
	mat[5] = cos(cam->orien_vec->x);
	return (mat);
}

static float	*translate_camera(t_camera *cam, float *to_world)
{
	float	*translate;
	float	*ret;

	translate = make_matrix();
	translate[3] = cam->pos->x;
	translate[7] = cam->pos->y;
	translate[11] = cam->pos->z;
	ret = matrix_multiply(to_world, translate);
	free(translate);
	return (ret);
}

float			*rotate_camera(t_camera *cam)
{
	float	*cam_to_world;
	float	*rotation_x;
	float	*rotation_y;
	float	*rotation_z;

	cam_to_world = make_matrix();
	rotation_x = create_rotation_x(cam);
	rotation_y = create_rotation_y(cam);
	rotation_z = create_rotation_z(cam);
	cam_to_world = matrix_multiply(cam_to_world, rotation_x);
	cam_to_world = matrix_multiply(cam_to_world, rotation_y);
	cam_to_world = matrix_multiply(cam_to_world, rotation_z);
	cam_to_world = translate_camera(cam, cam_to_world);
	free(rotation_x);
	free(rotation_y);
	free(rotation_z);
	return (cam_to_world);
}

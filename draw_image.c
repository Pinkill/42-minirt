/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_image.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 22:38:24 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/21 17:23:54 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	write_image_header(int bpp, int fd)
{
	char	header[54];

	ft_memset(header, 0, 54 * sizeof(char));
	header[0] = 'B';
	header[1] = 'M';
	header[10] = 54;
	header[14] = 40;
	header[18] = g_x_res & 0xff;
	header[19] = (g_x_res >> 8) & 0xff;
	header[20] = (g_x_res >> 16) & 0xff;
	header[21] = (g_x_res >> 24) & 0xff;
	header[22] = g_y_res & 0xff;
	header[23] = (g_y_res >> 8) & 0xff;
	header[24] = (g_y_res >> 16) & 0xff;
	header[25] = (g_y_res >> 24) & 0xff;
	header[26] = 1;
	header[28] = bpp;
	write(fd, header, 54);
}

void		set_pixel(char *buffer, t_color rgb, int i[2])
{
	buffer[i[0] * (g_x_res * 4) + i[1] * 4] = rgb.b;
	buffer[i[0] * (g_x_res * 4) + (i[1] * 4) + 1] = rgb.g;
	buffer[i[0] * (g_x_res * 4) + (i[1] * 4) + 2] = rgb.r;
}

void		set_pixel_ambient(char *buf, int i[2], t_all_objects *all)
{
	t_color	rgb;

	rgb.r = all->ambient_lcolor->r * g_ambient_light;
	rgb.g = all->ambient_lcolor->g * g_ambient_light;
	rgb.b = all->ambient_lcolor->b * g_ambient_light;
	set_pixel(buf, rgb, i);
}

int			store_picture(char *buffer, int bpp, int n, t_all_objects *all)
{
	char			img[n];
	int				i[2];
	int				var[3];

	ft_memset(img, 0, n * sizeof(char));
	if ((var[0] = open("image.bmp", O_CREAT | O_WRONLY | O_TRUNC, 0777)) < 1)
		return (-1);
	write_image_header(bpp, var[0]);
	i[0] = -1;
	while (++i[0] < g_y_res)
	{
		i[1] = -1;
		while (++i[1] < g_x_res)
		{
			var[1] = i[0] * (g_x_res * 4) + i[1] * 4;
			var[2] = (g_y_res - i[0]) * (g_x_res * 4) + i[1] * 4;
			img[var[1]] = buffer[var[2]];
			img[var[1] + 1] = buffer[var[2] + 1];
			img[var[1] + 2] = buffer[var[2] + 2];
		}
	}
	write(var[0], img, n);
	close(var[0]);
	exit_program(all);
	return (1);
}

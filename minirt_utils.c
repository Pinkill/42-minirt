/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 23:09:06 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/15 22:06:31 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

void	clear_shadow_ray(t_ray_data *data)
{
	if (data)
	{
		if (data->ray_o)
			free(data->ray_o);
		if (data->ray_dir)
			free(data->ray_dir);
		if (data->buffer)
			free(data->buffer);
		free(data);
	}
}

float	*calc_cam_origin(t_camera *cam, t_ray_data *data)
{
	float	*cam_to_world;

	cam_to_world = rotate_camera(cam);
	if (!(data->ray_o = malloc(sizeof(t_pos))))
		clear_shadow_ray(data);
	if (!(data->ray_dir = malloc(sizeof(t_pos))))
		clear_shadow_ray(data);
	if (data)
	{
		data->ray_o->x = 0;
		data->ray_o->y = 0;
		data->ray_o->z = 0;
		vector_matrix_multiply(data->ray_o, cam_to_world);
	}
	else
	{
		free(cam_to_world);
		return (0);
	}
	return (cam_to_world);
}

void	ft_default_minirt(t_minirt *minirt)
{
	minirt->mlx_ptr = 0;
	minirt->win_ptr = 0;
	minirt->img_ptr = 0;
}

int		exit_program(t_all_objects *all)
{
	clear_all(all);
	exit(0);
	return (0);
}

int		free_line(char **line, int ret)
{
	if (*line)
		free(*line);
	return (ret);
}

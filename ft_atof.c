/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 17:19:05 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/02 20:20:43 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static float	ft_decimals(char **str, float result)
{
	int decimal;

	decimal = 1;
	while (**str != '\0' && **str >= '0' && **str <= '9')
	{
		result += ((**str - '0') / (pow(10, decimal++)));
		*str += 1;
	}
	return (result);
}

float			ft_atof(char **str)
{
	float	result;
	int		minus;

	result = 0.0;
	minus = 1;
	while (ft_isspace(**str) == 1)
		*str += 1;
	if (**str == '-' || **str == '+')
	{
		if (**str == '-')
			minus = -1;
		*str += 1;
	}
	while (**str != '\0' && **str >= '0' && **str <= '9')
	{
		result *= 10;
		result += (**str - '0');
		*str += 1;
	}
	if (**str == '.')
		*str += 1;
	return (ft_decimals(str, result) * minus);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_camera.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/14 19:55:04 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/06 21:27:12 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	ft_default_camera(t_camera *camera)
{
	camera->next = 0;
	camera->prev = 0;
	camera->pos = 0;
	camera->orien_vec = 0;
	camera->fov = 0;
	camera->id = 0;
}

static int	ft_read_position(char **line, t_camera *camera)
{
	if (!(camera->pos = malloc(sizeof(t_pos))))
		return (0);
	camera->pos->x = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	camera->pos->y = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	camera->pos->z = ft_atof(line);
	return (1);
}

static int	ft_read_o_vec(char **line, t_camera *camera)
{
	if (!(camera->orien_vec = malloc(sizeof(t_pos))))
		return (0);
	camera->orien_vec->x = ft_atof(line);
	if (!(ft_check_comma(line)) || camera->orien_vec->x < -1.0
		|| camera->orien_vec->x > 1.0)
		return (0);
	camera->orien_vec->y = ft_atof(line);
	if (!(ft_check_comma(line)) || camera->orien_vec->y < -1.0
		|| camera->orien_vec->y > 1.0)
		return (0);
	camera->orien_vec->z = ft_atof(line);
	if (camera->orien_vec->z < -1.0
		|| camera->orien_vec->z > 1.0)
		return (0);
	camera->orien_vec->x *= M_PI;
	camera->orien_vec->y *= M_PI;
	camera->orien_vec->z *= M_PI;
	return (1);
}

static int	ft_find_last_id(t_camera *cameras)
{
	t_camera	*tmp;
	int			id;

	if (cameras == 0)
		return (0);
	tmp = cameras->next;
	id = cameras->id;
	while (id < tmp->id)
	{
		id = tmp->id;
		tmp = tmp->next;
	}
	return (id);
}

int			read_camera(char *line, t_all_objects *all)
{
	t_camera	*new;

	if (!(new = malloc(sizeof(t_camera))))
		return (0);
	ft_default_camera(new);
	if (all->cameras == 0)
	{
		new->next = new;
		new->prev = new;
	}
	new->id = ft_find_last_id(all->cameras) + 1;
	ft_camera_add(&all->cameras, new);
	if (!(ft_read_position(&line, new)))
		return (0);
	if (!(ft_read_o_vec(&line, new)))
		return (0);
	new->fov = ft_atoi((const char **)&line);
	if (new->fov > 180 || new->fov < 0)
		return (0);
	return (1);
}

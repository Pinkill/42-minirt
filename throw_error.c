/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   throw_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/15 14:38:57 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/10 22:45:22 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int		is_file_empty(char *file)
{
	int		fd;
	char	*buf;

	fd = 0;
	if (!(buf = malloc(sizeof(char))))
		return (1);
	if ((fd = open(file, O_RDONLY)) < 1)
		return (1);
	if (read(fd, buf, 1) < 1)
	{
		free(buf);
		close(fd);
		return (1);
	}
	if (buf)
		free(buf);
	close(fd);
	return (0);
}

void	clear_triangle(t_object *obj)
{
	if (obj->triangle)
	{
		if (obj->triangle->pos_0)
			free(obj->triangle->pos_0);
		if (obj->triangle->pos_1)
			free(obj->triangle->pos_1);
		if (obj->triangle->pos_2)
			free(obj->triangle->pos_2);
		free(obj->triangle);
	}
}

int		throw_error(t_all_objects *all, int errnum, int fd)
{
	char *message;

	if (all)
		clear_all(all);
	if (fd != 0)
		close(fd);
	message = strerror(errnum);
	perror("Error:\n");
	ft_putstr_fd(message, 1);
	ft_putstr_fd("\n", 1);
	return (0);
}

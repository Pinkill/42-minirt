/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersects_ad.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 22:26:06 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/14 15:42:55 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static int		solve_quadratic(float s[3], float t[2])
{
	float	discr;
	float	q;

	discr = s[1] * s[1] - 4 * s[0] * s[2];
	if (discr < 0)
		return (0);
	else if (discr == 0)
	{
		t[0] = -0.5 * s[1] / s[0];
		t[1] = t[0];
	}
	else
	{
		q = (s[1] > 0) ? -0.5 * (s[1] + sqrt(discr)) :
						-0.5 * (s[1] - sqrt(discr));
		t[0] = q / s[0];
		if (t[0] > s[2] / q)
		{
			t[1] = t[0];
			t[0] = s[2] / q;
		}
		else
			t[1] = s[2] / q;
	}
	return (1);
}

float			intersect_sphere(t_object *sp, t_pos *ray_o, t_pos *ray_d)
{
	float		s[3];
	float		t[2];
	t_pos		l;

	l = vector_subtract(*ray_o, *sp->pos);
	s[0] = dot_product(*ray_d, *ray_d);
	s[1] = 2 * dot_product(*ray_d, l);
	s[2] = dot_product(l, l) - pow(sp->diameter / 2.0, 2);
	if (!solve_quadratic(s, t))
		return (0);
	if (t[0] < 0)
	{
		t[0] = t[1];
		if (t[0] < 0)
			return (0);
	}
	return (t[0]);
}

static float	cyl_ad(t_object *cy, t_pos *ray_o, t_pos *ray_dir, float x)
{
	t_pos		point;
	t_pos		temp;
	t_pos		norm;
	float		s[4];

	if (x < 0)
		return (-1);
	norm = normalize(vector_subtract(*cy->top, *cy->pos));
	temp = vector_multiply(x, *ray_dir);
	point = vector_add(*ray_o, temp);
	s[0] = -(dot_product(norm, *cy->top));
	s[1] = dot_product(point, norm) + s[0];
	s[2] = dot_product(norm, norm);
	temp = vector_add(point, vector_multiply(-(s[1] / s[2]), norm));
	s[3] = points_distance(point, temp);
	temp = vector_add(*cy->pos, vector_subtract(temp, *cy->top));
	s[1] = points_distance(point, temp);
	if (s[3] > (cy->height) || s[1] > (cy->height))
		return (-1);
	return (x);
}

float			intersect_cylinder(t_object *cy, t_pos *ray_o, t_pos *ray_dir)
{
	t_pos	vec[2];
	float	s[8];
	float	x[2];

	vec[1] = normalize(vector_subtract(*cy->top, *cy->pos));
	vec[0] = vector_subtract(*ray_o, *cy->pos);
	s[0] = 1.0 / dot_product(vec[1], vec[1]);
	s[1] = dot_product(vec[0], vec[0]);
	s[2] = dot_product(vec[1], vec[0]);
	s[3] = dot_product(*ray_dir, vec[1]);
	s[4] = dot_product(*ray_dir, *ray_dir) - (pow(s[3], 2) * s[0]);
	s[5] = 2.0 * (dot_product(*ray_dir, vec[0])) - (2 * s[3] * s[2] * s[0]);
	s[6] = s[1] - pow(cy->diameter / 2, 2) - (pow(s[2], 2) * s[0]);
	s[7] = s[5] * s[5] - 4.0 * (s[4] * s[6]);
	x[0] = (-s[5] + sqrt(s[7])) / (2.0 * s[4]);
	x[1] = (-s[5] - sqrt(s[7])) / (2.0 * s[4]);
	if (s[7] >= 0 || x[1] >= 0 || x[0] >= 0)
	{
		s[4] = cyl_ad(cy, ray_o, ray_dir, x[1]);
		if (s[4] == -1 && x[0] >= 0)
			s[4] = cyl_ad(cy, ray_o, ray_dir, x[0]);
		return (s[4]);
	}
	return (-1);
}

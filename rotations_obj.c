/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotations_obj.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/04 20:02:21 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/14 15:25:07 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static float	*create_rotation_x(t_object *obj)
{
	float	*mat;

	mat = make_matrix();
	mat[5] = cos(obj->orien_vec->z);
	mat[6] = -sin(obj->orien_vec->z);
	mat[9] = sin(obj->orien_vec->z);
	mat[10] = cos(obj->orien_vec->z);
	return (mat);
}

static float	*create_rotation_y(t_object *obj)
{
	float	*mat;

	mat = make_matrix();
	mat[0] = cos(obj->orien_vec->y);
	mat[2] = sin(obj->orien_vec->y);
	mat[8] = -sin(obj->orien_vec->y);
	mat[10] = cos(obj->orien_vec->y);
	return (mat);
}

static float	*create_rotation_z(t_object *obj)
{
	float	*mat;

	mat = make_matrix();
	mat[0] = cos(obj->orien_vec->x);
	mat[1] = -sin(obj->orien_vec->x);
	mat[4] = sin(obj->orien_vec->x);
	mat[5] = cos(obj->orien_vec->x);
	return (mat);
}

static float	*translate_object(t_object *obj, float *to_world)
{
	float	*translate;
	float	*ret;

	translate = make_matrix();
	translate[3] = obj->pos->x;
	translate[7] = obj->pos->y;
	translate[11] = obj->pos->z;
	ret = matrix_multiply(to_world, translate);
	free(translate);
	return (ret);
}

float			*rotate_object(t_object *obj, int translate)
{
	float	*obj_to_world;
	float	*rotation_x;
	float	*rotation_y;
	float	*rotation_z;

	obj_to_world = make_matrix();
	rotation_x = create_rotation_x(obj);
	rotation_y = create_rotation_y(obj);
	rotation_z = create_rotation_z(obj);
	obj_to_world = matrix_multiply(obj_to_world, rotation_x);
	obj_to_world = matrix_multiply(obj_to_world, rotation_y);
	obj_to_world = matrix_multiply(obj_to_world, rotation_z);
	if (translate == 1)
		obj_to_world = translate_object(obj, obj_to_world);
	free(rotation_x);
	free(rotation_y);
	free(rotation_z);
	return (obj_to_world);
}

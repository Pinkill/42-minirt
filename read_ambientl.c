/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_ambientl.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 17:16:50 by mknezevi          #+#    #+#             */
/*   Updated: 2020/08/15 22:12:00 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

#if (__APPLE__)

static int	mlx_get_screen_size(void *mlx_ptr, int *sizex, int *sizey)
{
	void	*tmp;

	tmp = mlx_ptr;
	*sizex = 0;
	*sizey = 0;
	return (0);
}

#endif

static int	ft_read_colors(char *line, t_all_objects *all)
{
	if (!(all->ambient_lcolor = malloc(sizeof(t_color))))
		return (0);
	all->ambient_lcolor->r = ft_atoi((const char **)&line);
	if (!(ft_check_comma(&line)))
		return (0);
	if (all->ambient_lcolor->r < 0 || all->ambient_lcolor->r > 255)
		return (0);
	all->ambient_lcolor->g = ft_atoi((const char **)&line);
	if (!(ft_check_comma(&line)))
		return (0);
	if (all->ambient_lcolor->g < 0 || all->ambient_lcolor->g > 255)
		return (0);
	all->ambient_lcolor->b = ft_atoi((const char **)&line);
	if (all->ambient_lcolor->b < 0 || all->ambient_lcolor->b > 255)
		return (0);
	return (1);
}

int			read_ambientl(char *line, t_all_objects *all)
{
	if (g_ambient_light > 0)
		return (0);
	if (all->ambient_lcolor != 0)
		return (0);
	g_ambient_light = ft_atof(&line);
	if (g_ambient_light > 1.0 || g_ambient_light < 0.0)
		return (0);
	if (!(ft_read_colors(line, all)))
		return (0);
	return (1);
}

int			read_resolution(char *line, t_all_objects *all)
{
	int	tmp_x;
	int	tmp_y;

	if (g_x_res > 0 || g_y_res > 0)
		return (0);
	g_x_res = ft_atoi((const char **)&line);
	if (g_x_res < 1)
		return (0);
	g_y_res = ft_atoi((const char **)&line);
	if (g_y_res < 1)
		return (0);
	if (__APPLE__)
	{
		if (g_x_res > MAX_X_RES)
			g_x_res = MAX_X_RES;
		if (g_y_res > MAX_Y_RES)
			g_y_res = MAX_Y_RES;
	}
	else
	{
		mlx_get_screen_size(all->minirt->mlx_ptr, &tmp_x, &tmp_y);
		if (g_x_res > tmp_x || g_y_res > tmp_y)
			mlx_get_screen_size(all->minirt->mlx_ptr, &g_x_res, &g_y_res);
	}
	return (1);
}

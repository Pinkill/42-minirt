# 42 miniRT

A project in the 42 study program. This project is an introduction into ray tracing. The compiled program has the ability to render out simple shapes and light sources using ray tracing.

To see the documentation of the program and how it works, check "en.subject.pdf" file.

## Getting Started

So far the project only works for MacOs (version High Sierra and on) and Linux. Use proper make commands, depending on what OS you use.

When running the program, you have to feed it an rt file, which has all the information about the scene.

### Making an rt file

The rt file gives information to the program about the scene (positioning of the objects in 3D space, their color, their orientation, etc.). It must be of .rt extension.

Each element has to be in its own line. Example scenes are located in scenes folder.

These are the avaliable elements:

[] - value

Can only be used once:
```
R [integer] [integer]
```
- Resolution
- Width and Height

```
A [float 0.0-1.0] [integer 0-255],[integer 0-255],[integer 0-255]
```
- Ambient light
- Intensity, 0.0 to 1.0
- R,G,B colors, 0 to 255

Can be used multiple times:
```
c [float],[float],[float] [float -1.0-1.0],[float -1.0-1.0],[float -1.0-1.0] [integer 0-180]
```
- Camera
- XYZ position
- Orientation vector (ie. where the object is facing), -1.0 to 1.0, 1.0 = 180°
- Field of View, 0 - 180)

```
l [float],[float],[float] [float 0.0-1.0] [integer],[integer],[integer]
```
- Light
- XYZ position
- Brightness, 0.0 to 1.0
- R,G,B colors, 0 to 255

```
sp [float],[float],[float] [float >0] [integer],[integer],[integer]
```
- Sphere
- XYZ position
- Diameter, above 0
- R,G,B colors, 0 to 255

```
pl [float],[float],[float] [float -1.0-1.0],[float -1.0-1.0],[float -1.0-1.0] [integer],[integer],[integer]
```
- Plane
- XYZ position
- Orientation vector, -1.0 to 1.0, 1.0 = 180°
- R,G,B colors, 0 to 255

```
sq [float],[float],[float] [float -1.0-1.0],[float -1.0-1.0],[float -1.0-1.0] [float >0] [integer],[integer],[integer]
```
- Square
- XYZ position
- Orientation vector, -1.0 to 1.0, 1.0 = 180°
- Side size, above 0
- R,G,B colors, 0 to 255

```
cy [float],[float],[float] [float -1.0-1.0],[float -1.0-1.0],[float -1.0-1.0] [float >0] [float >0] [integer],[integer],[integer]
```
- Cylinder
- XYZ position
- Orientation vector, -1.0 to 1.0, 1.0 = 180°
- Diameter, above 0
- Side size, above 0
- R,G,B colors, 0 to 255

```
ty [float],[float],[float] [float],[float],[float] [float],[float],[float] [integer],[integer],[integer]
```
- Triangle
- XYZ position of first point
- XYZ position of second point
- XYZ position of third point
- R,G,B colors, 0 to 255

### Prerequisites

For linux and mac:

```
- gcc
- minilibx library (included)
```

### Installing

Getting the project:

```
git clone https://gitlab.com/Pinkill/42-minirt minirt
```

#### Building the project

```
make all (MacOs)
make all_lin (Linux)
```

#### Cleaning, rebuilding the project:

Removes the program, but not the objects

```
make clean
```

Cleans whole project

```
make fclean
```

Cleans and rebuilds whole project

```
make re (MacOs)
make re_lin (Linux)
```

### Running miniRT

Feeding the program an rt file will let it make the scene properly, but if you wish to save an image, you can use "--save".

If there are multiple cameras in the scene, you can move between them with left and right arrow key (takes a few seconds o render).

```
./miniRT example.rt [--save]
```

### Example image

![Example](example_image1.png?raw=true)

### Comments

This was a super fun project and I am not gonna lie, was a proud of myself at the end. If this helps anyone, you are free to use/ask.

Good luck!

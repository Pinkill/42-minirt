/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_elvalues.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 22:42:19 by matt              #+#    #+#             */
/*   Updated: 2020/08/05 21:49:50 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int	ft_read_elcolor(char **line, t_object *object)
{
	if (!(object->rgb = malloc(sizeof(t_color))))
		return (0);
	object->rgb->r = ft_atoi((const char **)line);
	if (!(ft_check_comma(line)))
		return (0);
	if (object->rgb->r < 0 || object->rgb->r > 255)
		return (0);
	object->rgb->g = ft_atoi((const char **)line);
	if (!(ft_check_comma(line)))
		return (0);
	if (object->rgb->g < 0 || object->rgb->g > 255)
		return (0);
	object->rgb->b = ft_atoi((const char **)line);
	if (object->rgb->b < 0 || object->rgb->b > 255)
		return (0);
	return (1);
}

int	ft_read_elposition(char **line, t_object *object)
{
	if (!(object->pos = malloc(sizeof(t_pos))))
		return (0);
	object->pos->x = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	object->pos->y = ft_atof(line);
	if (!(ft_check_comma(line)))
		return (0);
	object->pos->z = ft_atof(line);
	return (1);
}

int	ft_read_elo_vec(char **line, t_object *object)
{
	if (!(object->orien_vec = malloc(sizeof(t_pos))))
		return (0);
	object->orien_vec->x = ft_atof(line);
	if (!(ft_check_comma(line)) || object->orien_vec->x < -1.0
		|| object->orien_vec->x > 1.0)
		return (0);
	object->orien_vec->y = ft_atof(line);
	if (!(ft_check_comma(line)) || object->orien_vec->y < -1.0
		|| object->orien_vec->y > 1.0)
		return (0);
	object->orien_vec->z = ft_atof(line);
	if (object->orien_vec->z < -1.0
		|| object->orien_vec->z > 1.0)
		return (0);
	object->orien_vec->x *= M_PI;
	object->orien_vec->y *= M_PI;
	object->orien_vec->z *= M_PI;
	return (1);
}
